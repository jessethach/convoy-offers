import { connect } from 'react-redux'
import Layout from './Layout'
import { getOffers, paginateOffers } from '../../actions/index'
import { RootState } from '../../types'

function mapStateToProps({ offers }: RootState) {
  return { offers }
}

export default connect(
  mapStateToProps,
  { getOffers, paginateOffers }
)(Layout)
