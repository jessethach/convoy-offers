import React, { Component, SyntheticEvent } from 'react'
import { Offer, SortOptions } from '../../types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const fixedNum = 2
const fixedDateStr = 10

enum PageDirection {
  NEXT = 'next',
  PREV = 'prev',
}

interface State {
  clickedOption: SortOptions
  page: number
}

interface Props {
  /*tslint:disable:no-any */
  getOffers: (sortOptions?: SortOptions) => any
  paginateOffers: (page: number) => any
  /*tslint:enable:no-any */
  offers: Offer[]
}

class Offers extends Component<Props, State> {
  state = {
    clickedOption: SortOptions.PICKUP_DATE,
    page: 0,
  }

  componentDidMount() {
    this.props.getOffers()
  }

  handlePaginate = (e: SyntheticEvent) => {
    const direction = e.currentTarget.id
    const next = direction === PageDirection.NEXT
    const { page } = this.state
    let currentPage
    if (next) {
      currentPage = page + 1
    } else {
      currentPage = page - 1
      if (currentPage < 0) {
        return
      }
    }
    this.setState({ page: currentPage })
    this.props.paginateOffers(currentPage)
  }

  handleOnClickSort = (e: SyntheticEvent) => {
    const sortOption = e.currentTarget.id as SortOptions
    this.setState({ clickedOption: sortOption })
    if (this.state.clickedOption === sortOption) {
      this.props.getOffers()
    } else {
      this.props.getOffers(sortOption)
    }
  }

  resetPage = () => {
    this.setState({ page: 0 })
    this.props.getOffers()
  }

  renderOffers = () => {
    const { offers } = this.props
    if (offers && offers.length > 0) {
      return offers.map((offer: Offer, i) => {
        return (
          <tr key={i} className="table-row">
            <td>{`${offer.origin.city}, ${offer.origin.state}`} </td>
            <td>{`${offer.destination.city}, ${offer.destination.state}`}</td>
            <td>{offer.miles}</td>
            <td>{offer.origin.pickup.start.substr(0, fixedDateStr)}</td>
            <td>{offer.destination.dropoff.end.substr(0, fixedDateStr)}</td>
            <td>{offer.offer.toFixed(fixedNum)}</td>
          </tr>
        )
      })
    }
    return (
      <tr>
        <td>
          <FontAwesomeIcon spin data-qa="angle-down" className="icon" icon="spinner" size="2x" />
        </td>
      </tr>
    )
  }

  renderSortIcon(sortOption: SortOptions) {
    const angleDirection = sortOption === this.state.clickedOption ? 'angle-up' : 'angle-down'
    return (
      <FontAwesomeIcon data-qa="angle-down" className="sort-icon" icon={angleDirection} size="1x" />
    )
  }

  render() {
    return (
      <section className="container">
        <div className="columns">
          <div className="column is-1">
            <button
              id={PageDirection.PREV}
              onClick={this.handlePaginate}
              role="button"
              className="button is-outlined"
            >
              ← Prev
            </button>
          </div>
          <div className="column is-1">
            <button
              id={PageDirection.NEXT}
              onClick={this.handlePaginate}
              role="button"
              className="button is-outlined"
            >
              Next →
            </button>
          </div>
        </div>
        <div>
          <table className="table">
            <thead>
              <tr>
                <th className="is-size-5" id={SortOptions.ORIGIN} onClick={this.handleOnClickSort}>
                  Location {this.renderSortIcon(SortOptions.ORIGIN)}
                </th>
                <th
                  className="is-size-5"
                  id={SortOptions.DESTINATION}
                  onClick={this.handleOnClickSort}
                >
                  Destination (mi) {this.renderSortIcon(SortOptions.DESTINATION)}
                </th>
                <th className="is-size-5" id={SortOptions.MILES} onClick={this.handleOnClickSort}>
                  Distance {this.renderSortIcon(SortOptions.MILES)}
                </th>
                <th
                  className="is-size-5"
                  id={SortOptions.PICKUP_DATE}
                  onClick={this.handleOnClickSort}
                >
                  Pick up date {this.renderSortIcon(SortOptions.PICKUP_DATE)}
                </th>
                <th
                  className="is-size-5"
                  id={SortOptions.DROP_OFF_DATE}
                  onClick={this.handleOnClickSort}
                >
                  Drop off date {this.renderSortIcon(SortOptions.DROP_OFF_DATE)}
                </th>
                <th className="is-size-5" id={SortOptions.PRICE} onClick={this.handleOnClickSort}>
                  Price {this.renderSortIcon(SortOptions.PRICE)}
                </th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>Location</th>
                <th>Destination</th>
                <th>Distance</th>
                <th>Pick up date</th>
                <th>Drop off date</th>
                <th>Price</th>
              </tr>
            </tfoot>
            <tbody>{this.renderOffers()}</tbody>
          </table>
        </div>
      </section>
    )
  }
}

export default Offers
