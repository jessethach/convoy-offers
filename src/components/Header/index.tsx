import 'bulma/css/bulma.css'
import React, { SFC, Fragment } from 'react'

const Navigation: SFC = () => {
  return (
    <Fragment>
      <nav data-qa="navbar-comp" className="navbar" role="navigation" aria-label="main navigation">
        <div className="navbar-brand">
          <div className="navbar-item">Welcome</div>
        </div>
      </nav>
      <hr className="navbar-divider" />
    </Fragment>
  )
}

export default Navigation
