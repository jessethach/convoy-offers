import 'bulma/css/bulma.css'
import React, { SFC } from 'react'

const Footer: SFC = () => {
  return (
    <footer className="footer">
      <div className="content has-text-centered">
        <p>
          <strong>Convoy Offers</strong> by <a href="https://github.com/jessethach">Jesse Thach</a>.
        </p>
      </div>
    </footer>
  )
}

export default Footer
