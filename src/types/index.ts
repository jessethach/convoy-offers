export interface Offer {
  miles: string | number
  origin: Origin
  destination: Destination
  offer: number
}

export interface Origin {
  city: string
  state: string
  pickup: Pickup
}

export interface Pickup {
  start: string
  end: string
}

export interface Destination {
  city: string
  state: string
  dropoff: Dropoff
}

export interface Dropoff extends Pickup {}

export interface RootState {
  offers: Offer[]
}

export interface SortConfig {
  offset: number
  limit: number
  sort: SortOptions
  order: 'desc' | 'asc'
}

export enum SortOptions {
  PICKUP_DATE = 'pickupDate',
  DROP_OFF_DATE = 'dropoffDate',
  PRICE = 'price',
  ORIGIN = 'origin',
  DESTINATION = 'destination',
  MILES = 'miles',
}
