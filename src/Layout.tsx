import React, { Component } from 'react'
import './App.css'
import Navigation from './components/Header'
import Offers from './components/Offers'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faAngleDown, faAngleUp, faSpinner } from '@fortawesome/free-solid-svg-icons'
import Footer from './components/Footer'

library.add(faAngleDown, faAngleUp, faSpinner)

class App extends Component {
  public render() {
    return (
      <div className="App">
        <Navigation />
        <Offers />
        <Footer />
      </div>
    )
  }
}

export default App
