import * as constants from '../constants'
import axios, { AxiosPromise, AxiosResponse } from 'axios'
import { SortOptions, SortConfig } from '../types'

const endpoint = 'https://convoy-frontend-homework-api.herokuapp.com/offers'

export interface PaginateOffers {
  type: constants.SORT_OFFERS
  payload: AxiosPromise | AxiosResponse
}

export interface GetOffers {
  type: constants.GET_OFFERS
  payload: AxiosPromise | AxiosResponse
}

export function paginateOffers(page: number): PaginateOffers {
  const params = { offset: page }
  const request = axios.get(endpoint, { params })

  return { type: constants.SORT_OFFERS, payload: request }
}

export function getOffers(sort?: SortOptions): GetOffers {
  let params: Partial<SortConfig> | null
  if (sort) {
    params = {
      sort,
    }
  } else {
    params = null
  }
  const request = axios.get(endpoint, { params })

  return { type: constants.GET_OFFERS, payload: request }
}

export type OfferAction = PaginateOffers | GetOffers
