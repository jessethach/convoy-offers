import { OfferAction } from '../actions'
import { RootState } from '../types/index'
import { SORT_OFFERS, GET_OFFERS } from '../constants/index'
import { AxiosResponse } from 'axios'

const defaultState: RootState = {
  offers: [],
}

export function offersReducer(prevState: RootState, action: OfferAction): RootState {
  const state = prevState || defaultState
  let payload

  switch (action.type) {
    case GET_OFFERS:
      // A small hack to get around what Typescript thinks is a promise. Type needs correcting in Library
      payload = action.payload as AxiosResponse
      return { ...state, offers: payload.data }
      break
    case SORT_OFFERS:
      payload = action.payload as AxiosResponse
      return { ...state, offers: payload.data }
    default:
      return state
  }
}

export default offersReducer
