### View app here:

[Convoy Offers](https://convoy-offers.herokuapp.com/)

### Getting Started

- Ensure you have [Node](https://nodejs.org/en/)
- Clone this repo

  ```
  $ git clone git clone git clone https://jessethach@bitbucket.org/jessethach/convoy-offers.git
  ```

  - Install dependencies

  ```
  $ npm install
  ```

  - Run the app locally

  ```
  $ npm start
  ```

##### Tech

- React/Redux/TypeScript
- Bulma.io for CSS layout

#### MVP

- ~~Table of offers~~
- ~~Deploy to Heroku~~
- ~~Sort by column~~

#### Stretch Goals

- ~~Pagination~~
- Persist session into LocalStorage
- Add routes
- ~~Add Redux~~
